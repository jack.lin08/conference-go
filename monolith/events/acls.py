import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    payload = {
        "per_page": 1,
        "query": f"{city} {state}",
    }

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=header, params=payload)

    pict = json.loads(response.content)
    picture_info = {"picture_url": pict["photos"][0]["url"]}
    try:
        return picture_info
    except (KeyError, IndexError):
        return None


# q={city},{state}&}&appid={OPEN_WEATHER_API_KEY}
def get_weather(city, state):
    try:
        loc_query = f"{city},{state},840"
        url = f"http://api.openweathermap.org/geo/1.0/direct?q={loc_query}&appid={OPEN_WEATHER_API_KEY}"
        geocode_request = requests.get(url)
        geocode_data = json.loads(geocode_request.content)

        lat = geocode_data[0]["lat"]
        lon = geocode_data[0]["lon"]

        url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
        weather_response = requests.get(url2)
        data = json.loads(weather_response.content)
        weather = {
            "temp": data["main"]["temp"],
            "description": data["weather"][0]["description"],
        }

        return weather
    except (IndexError):
        return None
